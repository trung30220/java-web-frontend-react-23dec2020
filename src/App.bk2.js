import './App.css';
import axios from 'axios';
import {useState, useEffect} from 'react';

function App() {
  const [state, setState] = useState({});
  const [newName, setNewName] = useState("");
  const [newDes, setNewDes] = useState("");
  var url = 'http://localhost:8080/my-test/products?page=0&size=10';
  var urlDelete = 'http://localhost:8080/my-test/product/delete?id=';
  var urlSave = 'http://localhost:8080/my-test/product/save';

  const load = () => {
    axios.get(url).then(res => {
      console.log("res", res);
      state.data = res.data.data;
      setState({...state});
    })
  }

  useEffect(() => {
    if (state.data != null) return;
    load();
  });

  const deleteData = (abc) => {
    let id = abc.target.value;
    console.log("click", id);
    axios.get(urlDelete+id).then(res => {
      load();
    })
  }

  const addNew = () => {
    console.log("Add new item " + newName);
    const body = {name: newName, description: newDes};
    axios.post(urlSave, body).then(res => {
      load();
    });
  }

  return (
    <div>
      <div>aa</div>
      <div className="App">
        Input<br/>
        name<input type="text" value={newName}
              onChange={e => setNewName(e.target.value)}/><br/>
        description<input type="text" value={newDes} onChange={e => setNewDes(e.target.value)}/><br/>
        <button onClick={addNew}>Save</button><br/>
        HELLO
        {state.data &&
          state.data.map(
            (item, index) => 
            <div key={index}>
            <button key={index} onClick={deleteData} value={item.id}>
              {'Delete ' + item.name}</button><br/>
            </div>
          )
        }
      </div>
    </div>
  );
}

export default App;
