import './App.css';
import axios from 'axios';
import {useState, useEffect} from 'react';

function App() {
  const [state, setState] = useState({});
  const [newName, setNewName] = useState("");
  const [newDes, setNewDes] = useState("");
  var url = 'http://localhost:8080/my-test/products?page=0&size=10';
  var urlDelete = 'http://localhost:8080/my-test/product/delete?id=';
  var urlSave = 'http://localhost:8080/my-test/product/save';

  const load = () => {
    axios.get(url).then(res => {
      console.log("res", res);
      state.data = res.data.data;
      setState({...state});
    })
  }

  useEffect(() => {
    if (state.data != null) return;
    load();
  });

  const addNew = () => {
    console.log("Add new item " + newName);
    const body = {name: newName, description: newDes};
    axios.post(urlSave, body).then(res => {
      load();
    });
  }

  const deleteData = (abc) => {
    console.log("click", abc.target.value);
    var id = abc.target.value;
    console.log("url", urlDelete+id);
    axios.get(urlDelete+id).then(res => {
      console.log("delete success");
      load();
    });
  }

  return (
    <div>
      <div>aa</div>
      <div className="App">
      <input type="text" value={newDes} 
        onChange={event => setNewDes(event.target.value)}/>
      <input type="text" value={newName} onChange={event => setNewName(event.target.value)}/>
        <button onClick={addNew}>Add new</button>
        <br/>
        HELLO
        {state.data &&
          state.data.map(
            (item, index) => 
            <div>
              <button key={index} onClick={deleteData} value={item.id}>
                {'Delete ' + item.name}</button><br/>
            </div>
          )
        }
      </div>
    </div>
  );
}

export default App;
